[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/tboettjer/terraform-oci-intro)

# Getting started with Terraform on Oracle Cloud Infrastructure (OCI)

[Oracle Cloud Infrastructure (OCI)][cloud] is a second generation infrastructure as a service (IaaS) offering that combines the convenience of a cloud with the ability to control own resources. Dedicated infrastructure, compute, storage and network is used to empower logical resources, like hypervisors, container or network functions and higher level services like autonomous databases or software appliances. More than 20 data center regions enable operators to provide an optimal user experience and address regional privacy regulations, deploying data bases and applications close to a user.

[<img alt="Global Presence" src="https://www.oracle.com/a/ocom/img/cc01-oci-region-map-1075x450.png" title="Data Center Regions">][oci_regions]

[Terraform][terraform] is a deployment automation tool. Operations engineers use terraform to provision infrastructure that merge predefined cloud services with individual configuration templates. Terraform allows replace administrative processes with automated build plans. Operators describe mutual dependencies of service components in templates and execute build plans on infrastructure pools that can be addressed through a service provider. In the execution process Terraform aggregates granular plans that are developed in collaboration with developers who understand the internals of a particular applications and topology definitions that operators create to address non-functional sytem and communication requirements. 

Automated build plans represent an important step towards more agility and flexibilty in IT operations. Combining OCI's ability to provide infrastructure on demand with terraform capabilites to orchestrate the deployment process is an enabler for self-service as new interface between operators, service owners and developers in any IT organization. Service owners and developers are empowered to add, change or delete resources on demand within physically defined boundaries set by the operators. 

* ["Hello World"][hello]
* [Basic Settings][provider]
* [Identity and Access Management (IAM)][iam]
* [Networking][network]
* [Compute][compute]
* [Storage][storage]
* [Database][database]

This introduction builds on the [OCI training][training] and extends the official [Learning Path][learning]. After a short introduction we follow the same structure of the [Oracle Architect Associate][certification] certification training.


[<<][readme] | [<][readme] | [+][readme] | [>][hello] | [>>][links]

<!--- Links -->
[readme]: README.md
[hello]: docs/hello.md
[provider]: docs/provider.md
[iam]: docs/iam.md
[network]: docs/network.md
[compute]: docs/compute.md
[storage]: docs/storage.md
[database]: docs/database.md
[links]: docs/links.md

[cloud]: https://www.oracle.com/cloud/
[freetier]: http://signup.oraclecloud.com/
[training]: https://www.oracle.com/cloud/iaas/training/
[learning]: https://learn.oracle.com/ols/learning-path/become-oci-architect-associate/35644/75658
[certification]: https://www.oracle.com/cloud/iaas/training/architect-associate.html
[homeregion]: https://docs.cloud.oracle.com/en-us/iaas/Content/Identity/Tasks/managingregions.htm

[cloudshell]: https://docs.cloud.oracle.com/en-us/iaas/Content/API/Concepts/cloudshellintro.htm

[oci_doc]: https://docs.cloud.oracle.com/en-us/iaas/Content/API/SDKDocs/terraform.htm
[tf_doc]: https://registry.terraform.io/providers/hashicorp/oci/latest/docs
[cli_doc]: https://docs.cloud.oracle.com/en-us/iaas/tools/oci-cli/latest/oci_cli_docs/
[iam_doc]: https://docs.cloud.oracle.com/en-us/iaas/Content/Identity/Concepts/overview.htm
[network_doc]: https://docs.cloud.oracle.com/en-us/iaas/Content/Network/Concepts/overview.htm
[compute_doc]: https://docs.cloud.oracle.com/en-us/iaas/Content/Compute/Concepts/computeoverview.htm#Overview_of_the_Compute_Service
[storage_doc]: https://docs.cloud.oracle.com/en-us/iaas/Content/Object/Concepts/objectstorageoverview.htm
[database_doc]: https://docs.cloud.oracle.com/en-us/iaas/Content/Database/Concepts/databaseoverview.htm

[iam_video]: https://www.youtube.com/playlist?list=PLKCk3OyNwIzuuA-wq2rVuxUE13rPTvzQZ
[network_video]: https://www.youtube.com/playlist?list=PLKCk3OyNwIzvHm2E-cGrmoMes-VwanT3P
[compute_video]: https://www.youtube.com/playlist?list=PLKCk3OyNwIzsAjIaUaVsKdXcfBOy6LASv
[storage_video]: https://www.youtube.com/playlist?list=PLKCk3OyNwIzu7zNtt_w1dXFOUbAjheMeo
[database_video]: https://www.youtube.com/watch?v=F4-sxIsnbKI&list=PLKCk3OyNwIzsfuB9kj1CTPavjgByJBXGK

[jmespath_site]: https://jmespath.org/tutorial.html
[jq_site]: https://stedolan.github.io/jq/
[jq_play]: https://jqplay.org/
[json_validate]: https://jsonlint.com/

[vsc_site]: https://code.visualstudio.com/

[terraform]: https://www.terraform.io/
[tf_examples]: https://github.com/terraform-providers/terraform-provider-oci/tree/master/examples
[tf_lint]: https://www.hashicorp.com/blog/announcing-the-terraform-visual-studio-code-extension-v2-0-0

[oci_regions]: https://www.oracle.com/cloud/data-regions.html
